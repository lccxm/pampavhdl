library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.std_logic_arith.all;

entity tb is
end tb;

architecture tb of tb is
  signal clock : std_logic := '0';
  signal reset, pila_en, cancelar, pedido_en : std_logic := '0';
  signal pedido : std_logic_vector(3 downto 0) := (others => '0');
  signal pila : std_logic_vector(9 downto 0) := (others => '0');
  signal troco : std_logic_vector(9 downto 0);
  signal troco_en, verificando, preparando, pronto, cancelado : std_logic;

  type data is record
    cancelar  : std_logic;
    pila_en   : std_logic;
    pila      : std_logic_vector(9 downto 0);
    pedido_en : std_logic;
    pedido    : std_logic_vector(3 downto 0);
  end record;

  type input_data is array (natural range <>) of data;

  constant pedido_1 : input_data := 
  ( ('0', '1', conv_std_logic_vector(1000, 10), '0', "0000"),
    ('0', '1', conv_std_logic_vector(1000, 10), '0', "0000"),
    ('0', '1', conv_std_logic_vector(1000, 10), '0', "0000"),
    ('0', '0', conv_std_logic_vector(   0, 10), '0', "0000"),
    ('0', '1', conv_std_logic_vector( 500, 10), '0', "0000"),
    ('0', '1', conv_std_logic_vector( 200, 10), '0', "0000"),
    ('0', '1', conv_std_logic_vector(  50, 10), '0', "0000"),
    ('0', '0', conv_std_logic_vector(   0, 10), '1', "0101"), -- Pedido R$ 37,50: Fruki + Piazito Charrua (R$ 24,50)
    ('0', '0', conv_std_logic_vector(   0, 10), '0', "0000")
  );

  constant pedido_2 : input_data := 
  ( ('0', '1', conv_std_logic_vector( 500, 10), '0', "0000"),
    ('0', '1', conv_std_logic_vector( 200, 10), '0', "0000"),
    ('0', '0', conv_std_logic_vector(   0, 10), '0', "0000"),
    ('0', '0', conv_std_logic_vector(   0, 10), '1', "1111"), -- Pedido R$ 7,00: Fruki + Aipim + Macanudo (R$ 40,00)
    ('0', '0', conv_std_logic_vector(   0, 10), '0', "0000")
  ); 

  constant pedido_3 : input_data := 
  ( ('0', '1', conv_std_logic_vector(1000, 10), '0', "0000"),
    ('0', '1', conv_std_logic_vector(1000, 10), '0', "0000"),
    ('0', '1', conv_std_logic_vector(1000, 10), '0', "0000"),
    ('0', '1', conv_std_logic_vector( 500, 10), '0', "0000"),
    ('0', '1', conv_std_logic_vector( 100, 10), '0', "0000"),
    ('0', '0', conv_std_logic_vector(   0, 10), '1', "1011"), -- Pedido R$ 36,00: Fruki + Aipim + Galo Véio (R$ 36,00)
    ('0', '0', conv_std_logic_vector(   0, 10), '0', "0000")
  ); 

  constant pedido_4 : input_data := 
  ( ('0', '1', conv_std_logic_vector(1000, 10), '0', "0000"),
    ('0', '1', conv_std_logic_vector(  50, 10), '0', "0000"),
    ('1', '0', conv_std_logic_vector(   0, 10), '0', "0000"), -- Pedido R$ 10,50: Cancelado
    ('0', '0', conv_std_logic_vector(   0, 10), '0', "0000")
  ); 

begin

  reset <= '1', '0' after 3 ns;  -- reset
  clock <= not clock after 5 ns; -- clock com periodo de 10ns

  DUT : entity work.tp4
        port map (clock => clock, reset => reset, pila_en => pila_en,
                  pila => pila, cancelar => cancelar, pedido_en => pedido_en,
                  pedido => pedido, troco_en => troco_en, troco => troco,
                  verificando => verificando, preparando => preparando,
                  pronto => pronto, cancelado => cancelado );

  process --- leitura dos estimulos de entrada
  begin

    wait for 10 ns; -- espera para o inicio dos pedidos

    -- Pedido #1
    for i in 0 to pedido_1'high loop
      cancelar  <= pedido_1(i).cancelar;
      pila_en   <= pedido_1(i).pila_en;
      pila      <= pedido_1(i).pila;
      pedido_en <= pedido_1(i).pedido_en;
      pedido    <= pedido_1(i).pedido;
      wait for 10 ns;
    end loop;
    wait for 200 ns; -- espera para finalizar o pedido

    -- Pedido #2
    for i in 0 to pedido_2'high loop
      cancelar  <= pedido_2(i).cancelar;
      pila_en   <= pedido_2(i).pila_en;
      pila      <= pedido_2(i).pila;
      pedido_en <= pedido_2(i).pedido_en;
      pedido    <= pedido_2(i).pedido;
      wait for 10 ns;
    end loop;
    wait for 200 ns; -- espera para finalizar o pedido

    -- Pedido #3
    for i in 0 to pedido_3'high loop
      cancelar  <= pedido_3(i).cancelar;
      pila_en   <= pedido_3(i).pila_en;
      pila      <= pedido_3(i).pila;
      pedido_en <= pedido_3(i).pedido_en;
      pedido    <= pedido_3(i).pedido;
      wait for 10 ns;
    end loop;
    wait for 200 ns; -- espera para finalizar o pedido

    -- Pedido #4
    for i in 0 to pedido_4'high loop
      cancelar  <= pedido_4(i).cancelar;
      pila_en   <= pedido_4(i).pila_en;
      pila      <= pedido_4(i).pila;
      pedido_en <= pedido_4(i).pedido_en;
      pedido    <= pedido_4(i).pedido;
      wait for 10 ns;
    end loop;
    wait for 200 ns; -- espera para finalizar o pedido

    assert false
      report "A SIMULACAO TERMINOU!"
      severity failure;
  
   end process;

end tb;
