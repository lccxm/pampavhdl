--------------------------------------
-- Trabalho Pratico 4 - TP4
--
-- Aluno 1:
-- Aluno 2:
--------------------------------------
library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.std_logic_arith.all;
use IEEE.std_logic_unsigned.all;

entity tp4 is
	port(
		--		ENTRADAS
		pila_en     : in  std_logic;
		pila        : in  std_logic_vector(9 downto 0);
		cancelar    : in  std_logic;
		pedido_en   : in  std_logic;
		pedido      : in  std_logic_vector(3 downto 0);
		reset       : in  std_logic;
		clock       : in  std_logic;
		--		SAIDAS
		verificando : out std_logic;
		preparando  : out std_logic;
		pronto      : out std_logic;
		cancelado   : out std_logic;
		troco_en    : out std_logic;
		troco       : out std_logic_vector(9 downto 0)
	);

end tp4;

architecture tp4 of tp4 is
	---------------support signals-----------------------------------------------------------
	signal verificando_sig : std_logic                    := '0';
	signal preparando_sig  : std_logic                    := '0';
	signal pronto_sig      : std_logic                    := '0';
	signal cancelado_sig   : std_logic                    := '0';
	signal troco_en_sig    : std_logic                    := '0';
	signal troco_sig       : std_logic_vector(9 downto 0) := (others => '0');
	----------------------------------------------------------------------------------------
	------------------maq Estados-----------------------------------------------------------
	type FSM is (pilando_state, pedindo_state, verificando_state, preparando_state, pronto_state, cancelado_state, troco_state, fim_compra);
	signal state           : FSM                          := pilando_state;	
	-- Bits adicionais foram colocados no saldo para evitar a contagem de saldo negativo
	signal saldo         : std_logic_vector(15 downto 0) := (others => '0');
	signal tempo         : integer                       := 0;
	signal pedido_valor  : integer                       := 0;
	signal devolve_troco : std_logic_vector(15 downto 0)  := (others => '0');
	signal pedido_tmp	 : std_logic_vector(3 downto 0)  := (others => '0');
	constant VALOR_10 	 : std_logic_vector(9 downto 0):= "1111101000";
	constant VALOR_5 	 : std_logic_vector(9 downto 0):= "0111110100";
	constant VALOR_2 	 : std_logic_vector(9 downto 0):= "0011001000";
	constant VALOR_1 	 : std_logic_vector(9 downto 0):= "0001100100";
	constant VALOR_05 	 : std_logic_vector(9 downto 0):= "0000110010";
	
	
	----------------------------------------------------------------------------------------	
begin
	verificando <= verificando_sig;
	preparando  <= preparando_sig;
	pronto      <= pronto_sig;
	cancelado   <= cancelado_sig;
	troco_en    <= troco_en_sig;
	troco       <= troco_sig;

	pedido_valor <=      0	  when (pedido = "0000") and (pedido_en = '1')
	                else 200  when (pedido = "0001") and (pedido_en = '1')
	                else 600  when (pedido = "0010") and (pedido_en = '1')
	                else 800  when (pedido = "0011") and (pedido_en = '1')
	                else 2250 when (pedido = "0100") and (pedido_en = '1')
	                else 2450 when (pedido = "0101") and (pedido_en = '1')
	                else 2850 when (pedido = "0110") and (pedido_en = '1')
	                else 3050 when (pedido = "0111") and (pedido_en = '1')
	                else 2800 when (pedido = "1000") and (pedido_en = '1')
	                else 3000 when (pedido = "1001") and (pedido_en = '1')
	                else 3400 when (pedido = "1010") and (pedido_en = '1')
	                else 3600 when (pedido = "1011") and (pedido_en = '1')
	                else 3200 when (pedido = "1100") and (pedido_en = '1')
	                else 3400 when (pedido = "1101") and (pedido_en = '1')
	                else 3800 when (pedido = "1110") and (pedido_en = '1')
	                else 4000 when (pedido = "1111") and (pedido_en = '1')
	;
	
	process(clock)
	
	begin
			
		if rising_edge(clock) then
			if (reset = '1') then
				--			Sistema resetado, devemos inicializar os sinais
				verificando_sig <= '0';
				preparando_sig  <= '0';
				pronto_sig      <= '0';
				cancelado_sig   <= '0';
				troco_en_sig    <= '0';
				troco_sig       <= (others => '0');
				devolve_troco   <= (others => '0');
			else
				
				case state is
					when pilando_state =>
						if (pila_en = '1') then
							saldo <= saldo + pila;
						elsif (pedido_en = '1') then
							pedido_tmp <= pedido;
							verificando_sig <= '1';
							devolve_troco   <= (others => '0');
							state <= pedindo_state;
						elsif (cancelar = '1') then
							state <= troco_state;
							devolve_troco <= saldo;						
						end if;
						
					when pedindo_state => 
						-- Manter os sinais por 1 ciclo de clock.
						if (cancelar = '1') then
							if (saldo > 0) then
								state <= troco_state;
							else 
								state <= fim_compra;
							end if;
						else
							verificando_sig <= '0';						
							state <= verificando_state;
							preparando_sig <= '1';
							if (pedido_tmp = "0000") then
								tempo <= 0;
							elsif (pedido_tmp = "0001") then
								tempo <= 1;
							elsif (pedido_tmp = "0010") then
								tempo <= 2;
							elsif (pedido_tmp = "0011") then
								tempo <= 3;
							elsif (pedido_tmp = "0100") then
								tempo <= 4;
							elsif (pedido_tmp = "0101") then
								tempo <= 5;
							elsif (pedido_tmp = "0110") then
								tempo <= 6;
							elsif (pedido_tmp = "0111") then
								tempo <= 7;
							elsif (pedido_tmp = "1000") then
								tempo <= 8;
							elsif (pedido_tmp = "1001") then
								tempo <= 9;
							elsif (pedido_tmp = "1010") then
								tempo <= 10;
							elsif (pedido_tmp = "1011") then
								tempo <= 11;
							elsif (pedido_tmp = "1100") then
								tempo <= 12;
							elsif (pedido_tmp = "1101") then
								tempo <= 13;
							elsif (pedido_tmp = "1110") then
								tempo <= 14;
							elsif (pedido_tmp = "1111") then
								tempo <= 15;
							end if;
	         				pedido_tmp <= (others => '0');
	         			end if;
					when verificando_state =>
						if (cancelar = '1') then
							if (saldo > 0) then
								state <= troco_state;
							else 
								state <= fim_compra;
							end if;
						else
							if signed(saldo - pedido_valor) = 0 then
								saldo <= (others => '0'); -- valor exato
								tempo <= tempo - 1;	
								state <= preparando_state;
							elsif signed(saldo - pedido_valor) > 0 then -- sobra troco
								saldo <= saldo - pedido_valor; 
								tempo <= tempo - 1;	
								state <= preparando_state;
							elsif signed(saldo - pedido_valor) < 0 then 
							-- saldo insuficiente, devolver saldo
								state <= cancelado_state;
								devolve_troco <= saldo;						
							
							end if;
						end if;
					when preparando_state =>
						if (tempo > 0) then
							tempo <= tempo - 1;
						else
							preparando_sig <= '0';
							state <= pronto_state;
							pronto_sig <= '1';						
						end if;
					when pronto_state =>
						if (cancelar = '1') then
							if (saldo > 0) then
								state <= troco_state;
							else 
								state <= fim_compra;
							end if;
						else
							pronto_sig <= '0';
							if (saldo > 0) then
								devolve_troco <= saldo;
								state <= troco_state;
							else
								state <= fim_compra;
							end if;
						end if;
					when troco_state => -- logica do troco
							troco_en_sig <= '1';
							if (cancelado_sig = '1') then
								cancelado_sig <= '0';
 							end if;
							if signed(devolve_troco - 1000) > 0 then
								devolve_troco <= devolve_troco - 1000;
								saldo <= saldo - 1000;
								troco_sig <= VALOR_10;
							elsif signed(devolve_troco - 1000) = 0 then
								devolve_troco <= devolve_troco - 1000;
								saldo <= saldo - 1000;
								troco_sig <= VALOR_10;
								troco_en_sig <= '0';
								state <= fim_compra;
							
							elsif signed(devolve_troco - 500) > 0 then
								devolve_troco <= devolve_troco - 500;
								saldo <= saldo - 500;
								troco_sig <= VALOR_5;
							elsif signed(devolve_troco - 500) = 0 then
								devolve_troco <= devolve_troco - 500;
								saldo <= saldo - 500;
								troco_sig <= VALOR_5;
								troco_en_sig <= '0';
								state <= pilando_state;
						
							elsif signed(devolve_troco - 200) > 0 then
								devolve_troco <= devolve_troco - 200;
								saldo <= saldo - 200;
								troco_sig <= VALOR_2;
							elsif signed(devolve_troco - 200) = 0 then
								devolve_troco <= devolve_troco - 200;
								saldo <= saldo - 200;
								troco_sig <= VALOR_2;
								troco_en_sig <= '0';
								state <= fim_compra;
							
							elsif signed(devolve_troco - 100) > 0 then
								devolve_troco <= devolve_troco - 100;
								saldo <= saldo - 100;
								troco_sig <= VALOR_1;
							elsif signed(devolve_troco - 100) = 0 then
								devolve_troco <= devolve_troco - 100;
								saldo <= saldo - 100;
								troco_sig <= VALOR_1;
								troco_en_sig <= '0';
								state <= fim_compra;
							
							elsif signed(devolve_troco - 50) > 0 then
								devolve_troco <= devolve_troco - 50;
								saldo <= saldo - 50;
								troco_sig <= VALOR_05;
							elsif signed(devolve_troco - 50) = 0 then
								devolve_troco <= devolve_troco - 50;
								saldo <= saldo - 50;
								troco_sig <= VALOR_05;
								troco_en_sig <= '0';
								state <= fim_compra;
							end if;
					when cancelado_state =>
						cancelado_sig <= '1';
						if (saldo > 0) then
							state <= troco_state;
						else
							state <= fim_compra;
						end if;
						
					when fim_compra =>
						troco_sig <= (others => '0');
						state <= pilando_state;
						if (cancelado_sig = '1') then
							cancelado_sig <= '0';
						end if;
						
				end case;
			end if;
		end if;

	end process;
end architecture tp4;
